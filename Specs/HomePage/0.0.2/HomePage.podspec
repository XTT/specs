Pod::Spec.new do |s|
  s.name         = "HomePage"
  s.version      = "0.0.2"
  s.summary      = "框架"
  s.description  = "框架功能描述"
  s.homepage     = "git@bitbucket.org:videobrowser/homepage"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = "Tongtong Xu"
  s.platform     = :ios, "8.0"
  s.source       = { :git => "git@bitbucket.org:videobrowser/homepage.git", :tag => "#{s.version}" }
  s.default_subspecs = 'Code'
  s.subspec 'Code' do |ss|
    # ss.resource_bundles = {
    #   'LoadingIndicator' => ['BTKit/class/LoadingIndicator/LoadingIndicator.bundle/*.png'],
    # }
    ss.resources = 'Classes/**/*.{storyboard,xib}', 'Resources/**/*.{png,jpeg,jpg,,xcassets,plist}'
    ss.source_files = 'Classes/**/*.{h,m,swift}'
    ss.requires_arc = true
    # ss.dependency "Frameworks/Code/Toaster"
    # ss.dependency "BTFoundation/Code"
  end
  # s.subspec 'DLL' do |ss|
  #   ss.vendored_frameworks = 'Product/HomePage.framework'
  #   # ss.dependency "Frameworks/DLL/Toaster"
  #   # ss.dependency "BTFoundation/DLL"
  # end
  s.dependency 'KVOController'
  s.dependency 'SwiftyJSON'
  s.dependency 'SDWebImage'
  
end
