Pod::Spec.new do |s|
  s.name         = "WebView"
  s.version      = "0.0.1"
  s.summary      = "框架"
  s.description  = "框架功能描述"
  s.homepage     = "git@gitlab.breadtrip.com:ios/WebView"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = "Tongtong Xu"
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://XTT@bitbucket.org/videobrowser/webview.git"}
  s.default_subspecs = 'Code'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.0' }
  s.subspec 'Code' do |ss|
    # ss.resource_bundles = {
    #   'LoadingIndicator' => ['BTKit/class/LoadingIndicator/LoadingIndicator.bundle/*.png'],
    # }
    ss.resources = 'Classes/*.{storyboard,xib}', 'Resources/*.{png,jpeg,jpg,,xcassets}'
    ss.source_files = 'Classes/*.{h,m,swift}'
    ss.requires_arc = true
    # ss.dependency "Frameworks/Code/Toaster"
    # ss.dependency "BTFoundation/Code"
  end
  # s.subspec 'DLL' do |ss|
  #   ss.vendored_frameworks = 'Product/WebView.framework'
  #   # ss.dependency "Frameworks/DLL/Toaster"
  #   # ss.dependency "BTFoundation/DLL"
  # end
  s.dependency 'KVOController'
  s.dependency 'LGAlertView'
end
