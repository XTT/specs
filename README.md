# 面包旅行私有Specs仓库使用说明

### 1、克隆到本地

> pod repo add XTSpecs https://bitbucket.org/XTT/specs.git

这样在~/.cocoapods/repos/就可以看到我们私有的specs

### 2、在Podfile顶部添加specs仓库地址，如图

> source 'https://bitbucket.org/XTT/specs.git'

> source 'https://github.com/CocoaPods/Specs.git'

### 3、执行pod install 或者 pod update

# 创建私有pod库的方式参考：

http://www.cocoachina.com/ios/20150508/11785.html